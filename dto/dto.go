package dto

import "time"

type UserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type UserInfo struct {
	Id       int64  `json:"id"`
	Username string `json:"username"`
}

type LoginResponse struct {
	User  UserInfo `json:"user"`
	Token string   `json:"token"`
}

type Matches struct {
	Id       int64     `json:"id"`
	Opponent Opponent  `json:"opponent"`
	White    bool      `json:"white"`
	IsWin    bool      `json:"isWin"`
	Time     time.Time `json:"time"`
}

type Opponent struct {
	IsAuth   bool   `json:"isAuth"`
	Id       int64  `json:"id"`
	Username string `json:"username"`
}
