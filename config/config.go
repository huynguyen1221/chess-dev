package config

import (
	"os"

	"github.com/joho/godotenv"

	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Production bool `envconfig:"PRODUCTION" required:"false"`

	LoggerLevel    int    `envconfig:"LOGGER_LEVEL" required:"false" default:"1"`
	LoggerEncoding string `envconfig:"LOGGER_ENCODING" required:"false" default:"json"`

	ServerHost     string `envconfig:"SERVER_HOST" required:"false" default:""`
	ServerPort     string `envconfig:"SERVER_PORT" required:"false" default:"3000"`
	HttpServerPort string `envconfig:"HTTP_SERVER_PORT" required:"false" default:"80"`

	RedisHost     string `envconfig:"REDIS_HOST" required:"true"`
	RedisPort     string `envconfig:"REDIS_PORT" required:"true"`
	RedisPassword string `envconfig:"REDIS_PASSWORD" required:"true"`
	RedisPrefix   string `envconfig:"REDIS_PREFIX" required:"false" default:""`

	MysqlUser            string `envconfig:"MYSQL_USER" required:"true"`
	MysqlPassword        string `envconfig:"MYSQL_PASSWORD" required:"true"`
	MysqlHost            string `envconfig:"MYSQL_HOST" required:"true"`
	MysqlPort            string `envconfig:"MYSQL_PORT" required:"true"`
	MysqlDB              string `envconfig:"MYSQL_DB" required:"true"`
	MysqlConnectionLimit int    `envconfig:"MYSQL_CONNECTION_LIMIT" required:"true"`

	MysqlUserClass            string `envconfig:"MYSQL_USER_CLASS" required:"true"`
	MysqlPasswordClass        string `envconfig:"MYSQL_PASSWORD_CLASS" required:"true"`
	MysqlHostClass            string `envconfig:"MYSQL_HOST_CLASS" required:"true"`
	MysqlPortClass            string `envconfig:"MYSQL_PORT_CLASS" required:"true"`
	MysqlDBClass              string `envconfig:"MYSQL_DB_CLASS" required:"true"`
	MysqlConnectionLimitClass int    `envconfig:"MYSQL_CONNECTION_LIMIT_CLASS" required:"true"`

	JwtSecret string `envconfig:"JWT_SECRET" required:"true"`
}

var cfg *Config

func init() {
	env := os.Getenv("GO_ENV")
	if env == "" {
		env = "local"
	}
	// --- Load .env file
	_, ok := os.LookupEnv("GO_TEST")
	if ok {
		_ = godotenv.Load(os.ExpandEnv("$GOPATH/src/mooncity-socket-serrver/.env"))
	} else {
		_ = godotenv.Load() // Load original .env
		_ = godotenv.Load(".env." + env)
	}

	cfg = &Config{
		Production: true,
	}
	err := envconfig.Process("", cfg)
	if err != nil {
		panic(err)
	}
}

func GetConfig() *Config {
	return cfg
}
