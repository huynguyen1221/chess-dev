package mysql_repo

import (
	"database/sql"
	"fmt"
	"log"
	"mooncity-socket-server/config"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

var (
	mysqlDB      *sqlx.DB
	cfg          *config.Config
	mysqlDBClass *sqlx.DB
)

func init() {
	cfg = config.GetConfig()
	InitMySQL()
	InitMySQLClass()
}

func InitMySQL() {
	var err error
	mysqlConnectStr := fmt.Sprintf("%v:%v@tcp(%v)/%v?parseTime=true", cfg.MysqlUser, cfg.MysqlPassword, cfg.MysqlHost+":"+cfg.MysqlPort, cfg.MysqlDB)
	// logger.Info("Connect String: ", zap.String("mysqlConnectStr", mysqlConnectStr))
	mysqlDB, err = sqlx.Connect("mysql", mysqlConnectStr)
	if err != nil {
		log.Fatal("Can't connect to MySql: ", err)
	}

	// --- Limit connection mysql
	mysqlDB.SetMaxOpenConns(cfg.MysqlConnectionLimit)
	mysqlDB.SetMaxIdleConns(cfg.MysqlConnectionLimit)
	mysqlDB.SetConnMaxLifetime(5 * time.Minute)

	// --- Make sure connection is available
	err = mysqlDB.Ping()
	if err != nil {
		log.Fatal("MySql connection lost: ", err)
	}
}

func InitMySQLClass() {
	var err error
	mysqlConnectStrClass := fmt.Sprintf("%v:%v@tcp(%v)/%v?parseTime=true", cfg.MysqlUserClass, cfg.MysqlPasswordClass, cfg.MysqlHostClass+":"+cfg.MysqlPortClass, cfg.MysqlDBClass)
	// logger.Info("Connect String: ", zap.String("mysqlConnectStrClass", mysqlConnectStrClass))
	mysqlDBClass, err = sqlx.Connect("mysql", mysqlConnectStrClass)
	if err != nil {
		log.Fatal("Can't connect to MySqlClass: ", err)
	}

	// --- Limit connection mysql
	mysqlDBClass.SetMaxOpenConns(cfg.MysqlConnectionLimitClass)
	mysqlDBClass.SetMaxIdleConns(cfg.MysqlConnectionLimitClass)
	mysqlDBClass.SetConnMaxLifetime(5 * time.Minute)

	// --- Make sure connection is available
	err = mysqlDBClass.Ping()
	if err != nil {
		log.Fatal("MySqlClass connection lost: ", err)
	}
}

func GetInstance() *sqlx.DB {
	return mysqlDB
}

func Release() error {
	return mysqlDB.Close()
}

func GetInstanceClass() *sqlx.DB {
	return mysqlDBClass
}

func ReleaseClass() error {
	return mysqlDBClass.Close()
}

func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
