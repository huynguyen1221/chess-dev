package redis_repo

import (
	"mooncity-socket-server/config"
	"time"

	"github.com/go-redis/redis"
)

var (
	cfg     *config.Config
	redisDB *redis.Client
)

func init() {
	cfg = config.GetConfig()

	if cfg.RedisPassword != "" {
		redisDB = redis.NewClient(&redis.Options{
			Addr: cfg.RedisHost + ":" + cfg.RedisPort,
			// DB:           2,
			Password:     cfg.RedisPassword,
			DialTimeout:  10 * time.Second,
			ReadTimeout:  30 * time.Second,
			WriteTimeout: 30 * time.Second,
			PoolSize:     0, // set 0: poolsize = 5 * cpu core
			PoolTimeout:  2 * time.Minute,
		})
	} else {
		redisDB = redis.NewClient(&redis.Options{
			Addr: cfg.RedisHost + ":" + cfg.RedisPort,
			// DB:           2,
			DialTimeout:  10 * time.Second,
			ReadTimeout:  30 * time.Second,
			WriteTimeout: 30 * time.Second,
			PoolSize:     0, // set 0: poolsize = 5 * cpu core
			PoolTimeout:  2 * time.Minute,
		})
	}
}

func GetNewInstance() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr: cfg.RedisHost + ":" + cfg.RedisPort,
		// DB:           2,
		Password:     cfg.RedisPassword,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     0, // set 0: poolsize = 5 * cpu core
		PoolTimeout:  2 * time.Minute,
	})
}

func GetInstance() *redis.Client {
	return redisDB
}
