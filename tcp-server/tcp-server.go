package tcp_server

import (
	"errors"
	"fmt"
	"math/rand"
	"mooncity-socket-server/config"
	"mooncity-socket-server/constants"
	tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"
	"mooncity-socket-server/utils"
	"strconv"

	"mooncity-socket-server/logger"
	// chat_model "mooncity-socket-server/models/chat-model"
	// game_model "mooncity-socket-server/models/game-model"
	// user_model "mooncity-socket-server/models/user-model"
	// tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"
	redis_repo "mooncity-socket-server/repositories/redis-repo"
	// "mooncity-socket-server/utils"
	"net"
	"os"
	"sync"

	"github.com/go-redis/redis"
	"github.com/gofrs/uuid"
	"go.uber.org/zap"
	"google.golang.org/protobuf/proto"
)

var (
	cfg *config.Config
)

func init() {
	cfg = config.GetConfig()
}

type TcpHandlerClass struct {
	redisCli  *redis.Client
	tcpServer net.Listener
	connList  map[string]*Client
	roomList  map[int32]*Room

	connListMutex sync.RWMutex
	roomListMutex sync.RWMutex
}

func (s *TcpHandlerClass) Init() {
	s.redisCli = redis_repo.GetNewInstance()
	s.connList = make(map[string]*Client)
	s.roomList = make(map[int32]*Room)

	s.connListMutex = sync.RWMutex{}
	s.roomListMutex = sync.RWMutex{}

	s.startServer()
}

func (s *TcpHandlerClass) startServer() {
	var err error

	logger.Info("Starting TCP Server...")
	// --- Init server
	// Listen for incoming connections.
	s.tcpServer, err = net.Listen("tcp", cfg.ServerHost+":"+cfg.ServerPort)
	if err != nil {
		logger.Error("Error listening:", zap.Error(err))
		os.Exit(1)
	}
	logger.Info("Listening TCP on " + "localhost:" + cfg.ServerPort)

	for {
		// Listen for an incoming connection.
		conn, err := s.tcpServer.Accept()
		if err != nil {
			logger.Info("Error accepting: ", zap.Error(err))
			continue
		}

		// create client
		cli := s.newClient(conn)
		s.addClient(cli)

		// read data from client
		go cli.readInput()
	}
}

func (s *TcpHandlerClass) addClient(cli *Client) error {
	s.connListMutex.RLock()
	_, ok := s.connList[cli.Name]
	s.connListMutex.RUnlock()

	if ok {
		return errors.New("duplicate connection")
	}

	s.connListMutex.Lock()
	if cli.IsAuth {
		s.connList[strconv.Itoa(int(cli.User.Id))] = cli
	} else {
		s.connList[cli.Name] = cli
	}
	s.connListMutex.Unlock()
	return nil
}

func (s *TcpHandlerClass) getClient(name string) (cli *Client, ok bool) {
	s.connListMutex.RLock()
	cli, ok = s.connList[name]
	s.connListMutex.RUnlock()
	return
}

func (s *TcpHandlerClass) removeClient(name string) {
	defer utils.HandlePanic()

	s.connListMutex.Lock()
	delete(s.connList, name)
	s.connListMutex.Unlock()
}

func (s *TcpHandlerClass) newClient(conn net.Conn) *Client {
	return &Client{
		conn:   conn,
		Name:   "guest" + uuid.Must(uuid.NewV4()).String(),
		server: s,
		mutex:  sync.Mutex{},
	}
}

func (s *TcpHandlerClass) getNewMinRoom() (min int32) {
	min = 1

	for {
		_, ok := s.roomList[min]
		if !ok {
			break
		}
		min++
	}
	return
}

func (s *TcpHandlerClass) createRoom(cli *Client, name string, pass string) {
	defer utils.HandlePanic()

	s.roomListMutex.Lock()

	id := s.getNewMinRoom()

	room := Room{
		Id:          id,
		Name:        name,
		Password:    pass,
		Status:      constants.ROOM_STATUS_WAITING,
		FirstPlayer: cli,
		ReverseSide: false,
		mutex:       sync.RWMutex{},
	}

	s.roomList[id] = &room
	s.roomListMutex.Unlock()

	cli.mutex.Lock()
	cli.Room = &room
	cli.mutex.Unlock()

	createRoomRes := tcp_msg_proto.CreateRoomResponse{
		Status: true,
		Msg:    "create room success",
		Id:     room.Id,
		Name:   room.Name,
	}
	msgRes, _ := proto.Marshal(&createRoomRes)
	cli.send(constants.CMD_CREATE_ROOM, msgRes)
}

func (s *TcpHandlerClass) getRoom(id int32) *Room {
	s.roomListMutex.RLock()
	val, ok := s.roomList[id]
	s.roomListMutex.RUnlock()

	if !ok {
		return nil
	}
	return val
}

func (s *TcpHandlerClass) removeRoom(id int32) {
	s.roomListMutex.RLock()
	delete(s.roomList, id)
	s.roomListMutex.RUnlock()
}

func (s *TcpHandlerClass) getRoomListInfo(cli *Client) {
	defer utils.HandlePanic()

	var roomBoard tcp_msg_proto.RoomBoard
	fmt.Println("Room list: ")

	s.roomListMutex.RLock()
	for _, room := range s.roomList {
		fmt.Println(room)
		isPassWord := false
		if room.Password != "" {
			isPassWord = true
		}
		roomBoard.RoomList = append(roomBoard.RoomList, &tcp_msg_proto.RoomInfo{Id: room.Id, Name: room.Name, Status: int32(room.Status), IsPassword: isPassWord})
	}

	s.roomListMutex.RUnlock()

	msgRes, _ := proto.Marshal(&roomBoard)
	cli.send(constants.CMD_GET_ROOM_LIST, msgRes)
}

func (s *TcpHandlerClass) getPlayerListInfo(cli *Client) {
	defer utils.HandlePanic()

}

func (s *TcpHandlerClass) quickJoinRoom(cli *Client) {
	defer utils.HandlePanic()

	if len(s.roomList) == 0 {
		s.createRoom(cli, "", "")
		return
	}

	var rooms []*Room
	s.roomListMutex.RLock()
	for _, r := range s.roomList {
		fmt.Println(r)

		if r.Password == "" && r.Status == constants.ROOM_STATUS_WAITING {
			rooms = append(rooms, r)
		}
	}
	s.roomListMutex.RUnlock()

	length := len(rooms)
	fmt.Println("rooms: ", rooms, length)
	if length == 0 {
		s.createRoom(cli, "", "")
		return
	}

	room := rooms[rand.Intn(length)]
	room.joinRoom(cli, "")
}

func endLine() {
	fmt.Println()
	fmt.Println("end.")
	fmt.Println()
	fmt.Println()
}
