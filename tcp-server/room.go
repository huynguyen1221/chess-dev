package tcp_server

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"mooncity-socket-server/constants"
	tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"
	redis_repo "mooncity-socket-server/repositories/redis-repo"
	"mooncity-socket-server/utils"
	"strconv"
	"sync"

	match_model "mooncity-socket-server/models/match-model"

	"github.com/go-redis/redis"
	"google.golang.org/protobuf/proto"
)

type Room struct {
	Id       int32  `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
	Status   uint8  `json:"status"`

	FirstPlayer  *Client   `json:"firstPlayer"`
	SecondPlayer *Client   `json:"SecondPlayer"`
	ObserverList []*Client `json:"observerList"`

	ReverseSide bool    `json:"reverseSide"`
	Turn        *Client `json:"turn"`
	UndoReq     bool    `json:"undoReq"`

	mutex    sync.RWMutex
	redisCli *redis.Client
}

func (room *Room) GetFirstPlayer() *Client {
	room.mutex.RLock()
	player := room.FirstPlayer
	room.mutex.RUnlock()
	return player
}

func (room *Room) GetSecondPlayer() *Client {
	room.mutex.Lock()
	player := room.SecondPlayer
	room.mutex.Unlock()
	return player
}

func (room *Room) CheckPlayerInRoom(cli *Client) bool {
	firstPlayer := room.FirstPlayer
	secondPlayer := room.SecondPlayer

	if firstPlayer == cli || secondPlayer == cli {
		return true
	}
	return false
}

func (room *Room) GetOpponentPlayer(cli *Client) (opponent *Client, err error) {
	if !room.CheckPlayerInRoom(cli) {
		cli.setRoom(nil)
		return nil, errors.New("you are not in this room")
	}
	if room.FirstPlayer == cli {
		opponent = room.SecondPlayer
	} else {
		opponent = room.FirstPlayer
	}
	return
}

func (room *Room) GetRoomStatus() uint8 {
	room.mutex.RLock()
	status := room.Status
	room.mutex.RUnlock()

	return status
}

func (room *Room) GetRedisKey() string {
	redisKey := cfg.RedisPrefix + "/" + strconv.Itoa(int(room.Id))
	return redisKey
}

func (room *Room) joinRoom(cli *Client, pass string) (role uint8, opponent string, err error) {
	defer utils.HandlePanic()

	if room.Password != pass {
		joinRoomRes := tcp_msg_proto.JoinRoomResponse{Status: false, Msg: "Wrong Pass"}
		msgRes, _ := proto.Marshal(&joinRoomRes)
		cli.send(constants.CMD_JOIN_ROOM, msgRes)
		return
	}

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if room.SecondPlayer != nil {
		room.ObserverList = append(room.ObserverList, cli)
		role = constants.OBSERVER
		return
	}

	role = constants.PLAYER_2
	opponent = room.FirstPlayer.Name
	room.SecondPlayer = cli
	room.Status = constants.ROOM_STATUS_FULL

	cli.setRoom(room)

	go func(firstPlayer *Client) {
		// noti to another client
		notiRes := tcp_msg_proto.NotiPlayerJoinRoom{
			Name: cli.Name,
		}
		msgRes, _ := proto.Marshal(&notiRes)
		firstPlayer.send(constants.NOTI_PLAYER_JOIN_ROOM, msgRes)

		// response to clients
		joinRoomRes := tcp_msg_proto.JoinRoomResponse{
			Status:   true,
			Msg:      "join room success",
			RoomId:   room.Id,
			Role:     int32(role),
			Opponent: opponent,
			Reverse:  room.ReverseSide,
		}
		msgRes, _ = proto.Marshal(&joinRoomRes)
		cli.send(constants.CMD_JOIN_ROOM, msgRes)
	}(room.FirstPlayer)

	return
}

func (room *Room) leftRoom(cli *Client) error {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer func() {
		room.mutex.Unlock()
		cli.setRoom(nil)
	}()

	msgRes := utils.GetActionResponse(true, "You left room")

	if room.FirstPlayer == cli {
		if room.SecondPlayer == nil {
			cli.server.removeRoom(room.Id)
			cli.send(constants.CMD_LEFT_ROOM, msgRes)
			return nil
		} else {
			room.FirstPlayer = room.SecondPlayer
			room.SecondPlayer = nil
			room.Status = constants.ROOM_STATUS_WAITING

			// mess to other player
			msgRes, _ := proto.Marshal(&tcp_msg_proto.NotiPlayerLeftRoom{Msg: room.FirstPlayer.Name + " left room", Role: int32(constants.PLAYER_1)})
			room.FirstPlayer.send(constants.NOTI_PLAYER_LEFT_ROOM, msgRes)

			cli.send(constants.CMD_LEFT_ROOM, msgRes)
			return nil
		}
	}

	if room.SecondPlayer == cli {
		room.SecondPlayer = nil
		room.Status = constants.ROOM_STATUS_WAITING

		// mess to other player
		msgRes, _ := proto.Marshal(&tcp_msg_proto.NotiPlayerLeftRoom{Msg: room.FirstPlayer.Name + " left room", Role: int32(constants.PLAYER_1)})
		room.FirstPlayer.send(constants.NOTI_PLAYER_LEFT_ROOM, msgRes)

		cli.send(constants.CMD_LEFT_ROOM, msgRes)
		return nil
	}

	msgRes = utils.GetActionResponse(false, "You are not in any room")
	cli.send(constants.CMD_LEFT_ROOM, msgRes)

	return errors.New("you are not in room")
}

func (room *Room) kickOpponent(cli *Client) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if cli != room.FirstPlayer {
		msgRes := utils.GetActionResponse(false, "You are not room master")
		cli.send(constants.CMD_KICK_FROM_ROOM, msgRes)
		return
	}
	if room.Status == constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Cant kick. Game is playing")
		cli.send(constants.CMD_KICK_FROM_ROOM, msgRes)
		return
	}

	if room.SecondPlayer == nil {
		return
	}

	secondPlayer := room.SecondPlayer

	room.SecondPlayer.Room = nil
	room.SecondPlayer = nil
	room.Status = constants.ROOM_STATUS_WAITING

	go func(aCli *Client) {
		aCli.send(constants.NOTI_BE_KICK_FROM_ROOM, []byte{})

		msgRes := utils.GetActionResponse(true, "kick successfully")
		cli.send(constants.CMD_KICK_FROM_ROOM, msgRes)
	}(secondPlayer)
}

func (room *Room) chat(cli *Client, msg []byte) {
	defer utils.HandlePanic()

	room.mutex.RLock()
	aCli, err := room.GetOpponentPlayer(cli)
	room.mutex.RUnlock()
	if err != nil {
		return
	}

	if aCli == nil {
		return
	}

	aCli.send(constants.CMD_CHAT_IN_ROOM, msg)
}

func (room *Room) readyGame(cli *Client) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if room.Status != constants.ROOM_STATUS_FULL {
		msgRes := utils.GetActionResponse(false, "cant ready.Status: "+strconv.Itoa(int(room.Status)))
		cli.send(constants.CMD_READY_GAME, msgRes)
		return
	}

	secondPlayer := room.SecondPlayer
	if secondPlayer != cli {
		msgRes := utils.GetActionResponse(false, "Cant ready. You are not second player")
		cli.send(constants.CMD_READY_GAME, msgRes)
		return
	}

	room.Status = constants.ROOM_STATUS_READY

	go func(firstPlayer *Client) {
		// response to client
		msgRes := utils.GetActionResponse(true, "Ready successfully")
		cli.send(constants.CMD_READY_GAME, msgRes)

		firstPlayer.send(constants.NOTI_PLAYER_READY_GAME, []byte{})
	}(room.FirstPlayer)
}

func (room *Room) startGame(cli *Client) {
	defer utils.HandlePanic()

	firstPlayer := room.GetFirstPlayer()

	if firstPlayer != cli {
		msgRes := utils.GetActionResponse(false, "you are not room master")
		cli.send(constants.CMD_START_GAME, msgRes)
		return
	}

	status := room.GetRoomStatus()
	if status != constants.ROOM_STATUS_READY {
		msgRes := utils.GetActionResponse(false, "Cant start game.Status: "+strconv.Itoa(int(room.Status)))
		cli.send(constants.CMD_START_GAME, msgRes)
		return
	}
	// start game
	room.mutex.Lock()
	room.redisCli = redis_repo.GetInstance()
	room.Status = constants.ROOM_STATUS_PLAYING
	room.UndoReq = false
	if room.ReverseSide {
		room.Turn = room.SecondPlayer
	} else {
		room.Turn = room.FirstPlayer
	}

	redisKey := room.GetRedisKey()
	room.redisCli.Del(redisKey)

	room.mutex.Unlock()

	go func(secondPlayer *Client) {
		msgRes := utils.GetActionResponse(true, "Game started")
		cli.send(constants.CMD_START_GAME, msgRes)

		secondPlayer.send(constants.NOTI_START_GAME, []byte{})
	}(room.SecondPlayer)
}

func (room *Room) moveGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	firstPlayer := room.FirstPlayer
	secondPlayer := room.SecondPlayer
	turn := room.Turn

	if cli != firstPlayer && cli != secondPlayer {
		msgRes := utils.GetActionResponse(false, "You are not player")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	if turn != cli {
		msgRes := utils.GetActionResponse(false, "It is not your turn")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	if turn == firstPlayer {
		room.Turn = secondPlayer
	} else {
		room.Turn = room.FirstPlayer
	}

	if room.UndoReq {
		room.UndoReq = false
	}
	redisKey := room.GetRedisKey()
	room.redisCli.RPush(redisKey, data)

	go func(aCli *Client) {
		aCli.send(constants.NOTI_MOVE_GAME, data)

		msgRes := utils.GetActionResponse(true, "")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
	}(room.Turn)
}

func (room *Room) undoMoveRequest(cli *Client, data []byte) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	aCli, err := room.GetOpponentPlayer(cli)
	turn := room.Turn

	if err != nil {
		res := utils.GetActionResponse(false, err.Error())
		cli.send(constants.CMD_UNDO_MOVE_REQUEST, res)
		return
	}

	if aCli == nil {
		res := utils.GetActionResponse(false, "Dont have opponent")
		cli.send(constants.CMD_UNDO_MOVE_REQUEST, res)
		return
	}

	if turn == cli {
		res := utils.GetActionResponse(false, "Cant request undo")
		cli.send(constants.CMD_UNDO_MOVE_REQUEST, res)
		return
	}

	redisKey := room.GetRedisKey()
	length, err := room.redisCli.LLen(redisKey).Result()
	if err != nil || length == 0 {
		res := utils.GetActionResponse(false, "You have not move")
		cli.send(constants.CMD_UNDO_MOVE_REQUEST, res)
		return
	}

	room.UndoReq = true

	// respose to all clients
	go func() {
		aCli.send(constants.NOTI_UNDO_MOVE_REQUEST, []byte{})

		res := utils.GetActionResponse(true, "Request success")
		cli.send(constants.CMD_UNDO_MOVE_REQUEST, res)
	}()
}

func (room *Room) undoMoveResponse(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if len(data) < 1 {
		res := utils.GetActionResponse(false, "Missing res")
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)
		return
	}

	room.mutex.Lock()
	defer room.mutex.Unlock()

	aCli, err := room.GetOpponentPlayer(cli)

	if err != nil {
		res := utils.GetActionResponse(false, err.Error())
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)
		return
	}

	if aCli == nil {
		res := utils.GetActionResponse(false, "Dont have opponent")
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)
		return
	}

	if !room.UndoReq || room.Turn != cli {
		res := utils.GetActionResponse(false, "Undo failed!")
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)
		return
	}

	if data[0] == 0 {
		room.UndoReq = false
		res := utils.GetActionResponse(true, "")
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)

		res = utils.GetActionResponse(false, "not accepted")
		aCli.send(constants.NOTI_UNDO_MOVE_RESPONSE, res)
		return
	}

	room.UndoReq = false
	room.Turn = aCli

	redisKey := room.GetRedisKey()
	room.redisCli.RPop(redisKey)

	go func() {
		res := utils.GetActionResponse(true, "Undo accepted")
		aCli.send(constants.NOTI_UNDO_MOVE_RESPONSE, res)

		res = utils.GetActionResponse(true, "Undo success")
		cli.send(constants.CMD_UNDO_MOVE_RESPONSE, res)
	}()
}

func (room *Room) surrenderGame(cli *Client) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if cli != room.FirstPlayer && cli != room.SecondPlayer {
		msgRes := utils.GetActionResponse(false, "You are not player")
		cli.send(constants.CMD_SURRENDER_GAME, msgRes)
		return
	}

	if room.Status != constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game has not started yet!")
		cli.send(constants.CMD_SURRENDER_GAME, msgRes)
		return
	}
	var winner uint8
	var aCli *Client
	if cli == room.FirstPlayer {
		winner = 2
		aCli = room.SecondPlayer
	} else {
		winner = 1
		aCli = room.FirstPlayer
	}

	// store movement to db
	redisKey := room.GetRedisKey()
	movementRedis, err := room.redisCli.LRange(redisKey, 0, -1).Result()
	if err == nil && (room.FirstPlayer.IsAuth || room.SecondPlayer.IsAuth) {
		player1 := sql.NullInt64{}
		player2 := sql.NullInt64{}
		if room.FirstPlayer.IsAuth {
			player1.Valid = true
			player1.Int64 = room.FirstPlayer.User.Id
		}
		if room.SecondPlayer.IsAuth {
			player2.Valid = true
			player2.Int64 = room.SecondPlayer.User.Id
		}

		movements, _ := json.MarshalIndent(movementRedis, "", "")
		match_model.Create(
			player1,
			player2,
			room.ReverseSide,
			sql.NullString{String: string(movements), Valid: true},
			winner)
	}

	room.Status = constants.ROOM_STATUS_FULL

	// response
	msgRes := utils.GetActionResponse(true, "succeed surrender")
	cli.send(constants.CMD_SURRENDER_GAME, msgRes)

	aCli.send(constants.NOTI_SURRENDER_GAME, []byte("opponent surrender"))

	// remove rediskey
	room.redisCli.Del(redisKey)
	room.redisCli = nil

}

func (room *Room) winGame(cli *Client) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if cli != room.FirstPlayer && cli != room.SecondPlayer {
		msgRes := utils.GetActionResponse(false, "You are not player")
		cli.send(constants.CMD_WIN_GAME, msgRes)
		return
	}

	if room.Status != constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game has not started yet!")
		cli.send(constants.CMD_WIN_GAME, msgRes)
		return
	}

	var winner uint8
	var aCli *Client
	if cli == room.FirstPlayer {
		winner = 1
		aCli = room.SecondPlayer
	} else {
		winner = 2
		aCli = room.FirstPlayer
	}

	// store movement to db
	redisKey := room.GetRedisKey()
	movementRedis, err := room.redisCli.LRange(redisKey, 0, -1).Result()
	if err == nil {
		player1 := sql.NullInt64{}
		player2 := sql.NullInt64{}
		if room.FirstPlayer.IsAuth {
			player1.Valid = true
			player1.Int64 = room.FirstPlayer.User.Id
		}
		if room.SecondPlayer.IsAuth {
			player2.Valid = true
			player2.Int64 = room.SecondPlayer.User.Id
		}

		movements, _ := json.MarshalIndent(movementRedis, "", "")
		fmt.Println("movements: ", string(movements))
		match_model.Create(
			player1,
			player2,
			room.ReverseSide,
			sql.NullString{String: string(movements), Valid: true},
			winner)
	}

	room.Status = constants.ROOM_STATUS_FULL

	// response
	msgRes := utils.GetActionResponse(true, "You win the game")
	cli.send(constants.CMD_WIN_GAME, msgRes)

	aCli.send(constants.NOTI_LOST_GAME, []byte("You lost the game"))

	// remove redis key
	room.redisCli.Del(redisKey)
	room.redisCli = nil
}

func (room *Room) changeSide(cli *Client) (reverse bool, err error) {
	defer utils.HandlePanic()

	room.mutex.Lock()
	defer room.mutex.Unlock()

	if cli != room.FirstPlayer {
		msgRes := utils.GetActionResponse(false, "You are not room master")
		cli.send(constants.CMD_CHANGE_SIDE_GAME, msgRes)
		return
	}

	if room.Status == constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game is playing")
		cli.send(constants.CMD_CHANGE_SIDE_GAME, msgRes)
		return
	}
	room.ReverseSide = !room.ReverseSide
	reverse = room.ReverseSide
	if room.Status == constants.ROOM_STATUS_READY {
		room.Status = constants.ROOM_STATUS_FULL
	}

	other, _ := cli.Room.GetOpponentPlayer(cli)

	go func() {
		// *** noti to opponent
		var msgRes []byte
		if other != nil {
			notiChangeSideGame := tcp_msg_proto.NotiChangeSideGame{Reverse: reverse}
			msgRes, _ = proto.Marshal(&notiChangeSideGame)
			other.send(constants.NOTI_CHANGE_SIDE_GAME, msgRes)
		}

		// response to client
		changeSideGameResponse := tcp_msg_proto.ChangeSideGameResponse{Status: true, Msg: "change side successfully", Reverse: reverse}
		msgRes, _ = proto.Marshal(&changeSideGameResponse)
		cli.send(constants.CMD_CHANGE_SIDE_GAME, msgRes)
	}()
	return
}
