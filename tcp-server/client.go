package tcp_server

import (
	"encoding/binary"
	"errors"
	"fmt"
	"mooncity-socket-server/constants"
	"mooncity-socket-server/logger"
	user_model "mooncity-socket-server/models/user-model"
	"mooncity-socket-server/utils"
	"net"
	"strconv"
	"sync"

	"github.com/gofrs/uuid"
	"go.uber.org/zap"
)

type Client struct {
	conn   net.Conn
	User   *user_model.User
	Name   string
	IsAuth bool

	Room *Room

	server *TcpHandlerClass
	mutex  sync.Mutex
}

func (cli *Client) readInput() {
	logger.Info("New Connection!")
	defer logger.Info("Connection closed!")
	defer cli.conn.Close()

	// add conn to list
	cli.server.connList[cli.Name] = cli

	// listen data
	for {
		data := make([]byte, constants.MAX_DATA_LENGTH_READ)

		length, err := cli.conn.Read(data)
		fmt.Println("clients: ", cli.server.connList)

		if err != nil {
			logger.Error("Read message error", zap.Error(err))
			// cli.server.
			cli.server.removeClient(cli.Name)
			break
		}

		data = data[:length]
		if length <= 1 {
			break
		}

		fmt.Println("------------------------------------------")
		fmt.Println()

		for {
			length := len(data)
			fmt.Println(data, length)

			if length < 4 {
				endLine()
				break
			}

			msgCMD := binary.LittleEndian.Uint16(data[:2])
			// logger.Info("", zap.Uint16("msgCMD", msgCMD))
			fmt.Println("msgCMD: ", msgCMD)

			msglen := binary.LittleEndian.Uint16(data[2:4])
			// logger.Info("", zap.Uint16("msglen", msglen))
			fmt.Println("msglen: ", msglen)

			if int(msglen)+4 > length {
				logger.Error("exceed msg length")
				endLine()
				break
			}

			msg := data[4 : msglen+4]

			switch msgCMD {
			case constants.CMD_AUTHEN:
				handleCMDAuthen(cli, msg)
			case constants.CMD_LOGOUT:
				handleLogout(cli)

			case constants.CMD_GET_ROOM_LIST:
				handleGetRoomList(cli, msg)
			case constants.CMD_CREATE_ROOM:
				hanldeCreateRoom(cli, msg)

			case constants.CMD_JOIN_ROOM:
				handleJoinRoom(cli, msg)
			case constants.CMD_QUICK_JOIN_ROOM:
				handleQuickJoinRoom(cli, msg)
			case constants.CMD_LEFT_ROOM:
				handleLeftRoom(cli, msg)
			case constants.CMD_KICK_FROM_ROOM:
				handleKickFromRoom(cli, msg)
			case constants.CMD_CHAT_IN_ROOM:
				handleChatInRoom(cli, msg)

			case constants.CMD_CHANGE_SIDE_GAME:
				handleChangSideGame(cli, msg)

			case constants.CMD_READY_GAME:
				handleReadyGame(cli, msg)
			case constants.CMD_START_GAME:
				handleStartGame(cli, msg)
			case constants.CMD_MOVE_GAME:
				handleMoveGame(cli, msg)
			case constants.CMD_UNDO_MOVE_REQUEST:
				handleUndoMoveRequest(cli, msg)
			case constants.CMD_UNDO_MOVE_RESPONSE:
				handleUndoMoveResponse(cli, msg)
			case constants.CMD_SURRENDER_GAME:
				handleSurrenderGame(cli, msg)
			case constants.CMD_WIN_GAME:
				handleWinGame(cli, msg)

			// case constants.CMD_GET_PLAYER_LIST:
			// 	handleGetPlayerList(cli, msg)
			default:
				cli.send(constants.CMD_ERROR, []byte("Wrong command"))
			}

			data = data[msglen+4:]

			endLine()
		}
	}
}

func (cli *Client) setAuthen(userId int32) (err error) {
	if cli.IsAuth {
		return errors.New("please logout before you login")
	}

	cli.User, err = user_model.GetById(int64(userId))
	if err != nil {
		return errors.New("user not found")
	}

	cli.IsAuth = true

	// change cli name
	cli.server.removeClient(cli.Name)
	cli.Name = cli.User.Username
	cli.server.addClient(cli)

	return nil
}

func (cli *Client) logout() {
	if cli.IsAuth {
		cli.IsAuth = false
		cli.server.removeClient(strconv.Itoa(int(cli.User.Id)))
		cli.Name = "guest" + uuid.Must(uuid.NewV4()).String()
		cli.server.addClient(cli)

		cli.User = nil
	}
}

func (cli *Client) setRoom(room *Room) {
	cli.mutex.Lock()
	cli.Room = room
	cli.mutex.Unlock()
}

func (cli *Client) send(cmd uint16, msg []byte) {
	resData := utils.CreateResTCPMsg(cmd, msg)
	cli.conn.Write(resData)
}
