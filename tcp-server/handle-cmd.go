package tcp_server

import (
	"mooncity-socket-server/constants"
	"mooncity-socket-server/utils"
	"strconv"

	tcp_msg_proto "mooncity-socket-server/protobuf/tcp-msg-proto"

	"google.golang.org/protobuf/proto"
)

func handleCMDAuthen(cli *Client, token []byte) {
	defer utils.HandlePanic()

	var err error
	var msgRes []byte

	claims, err := utils.ParseJWT(string(token))
	if err != nil {
		msgRes = utils.GetActionResponse(false, "token wrong")

		cli.send(constants.CMD_AUTHEN, msgRes)
		return
	}

	userId := (*claims).UserID

	// check user already logged in
	anotherCli, ok := cli.server.getClient(strconv.Itoa(int(userId)))

	if ok {
		msgRes = utils.GetActionResponse(false, "You 've logged in on another device")
		cli.send(constants.CMD_AUTHEN, msgRes)

		msgRes = utils.GetActionResponse(false, "someone login")
		anotherCli.send(constants.NOTI_SOMEONE_LOGIN, msgRes)

		// kick main conn
		go cli.server.removeClient(strconv.Itoa(int(userId)))

		anotherCli.conn.Close()
		return
	}

	// get user data
	err = cli.setAuthen(int32(userId))
	if err != nil {
		msgRes = utils.GetActionResponse(false, err.Error())
		cli.send(constants.CMD_AUTHEN, msgRes)
	}

	msgRes = utils.GetActionResponse(true, cli.Name)
	cli.send(constants.CMD_AUTHEN, msgRes)
}

func handleLogout(cli *Client) {
	defer utils.HandlePanic()
	cli.logout()

	res := utils.GetActionResponse(true, cli.Name)
	cli.send(constants.CMD_LOGOUT, res)
}

func hanldeCreateRoom(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes []byte
		err    error

		createRoomReq tcp_msg_proto.CreateRoomRequest
		createRoomRes tcp_msg_proto.CreateRoomResponse
	)

	if cli.Room != nil {
		createRoomRes = tcp_msg_proto.CreateRoomResponse{Status: false, Msg: "You are in another room"}
		msgRes, _ = proto.Marshal(&createRoomRes)
		cli.send(constants.CMD_CREATE_ROOM, msgRes)
		return
	}

	err = proto.Unmarshal(data, &createRoomReq)
	if err != nil {
		utils.ResponseWrongStruct(cli.conn, constants.CMD_CREATE_ROOM)
		return
	}

	// create room with min id
	go cli.server.createRoom(cli, createRoomReq.Name, createRoomReq.Password)
}

func handleJoinRoom(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes []byte
		err    error

		joinRoomReq tcp_msg_proto.JoinRoomRequest
		joinRoomRes tcp_msg_proto.JoinRoomResponse
	)

	if cli.Room != nil {
		joinRoomRes = tcp_msg_proto.JoinRoomResponse{Status: false, Msg: "You are in another room"}
		msgRes, _ = proto.Marshal(&joinRoomRes)
		cli.send(constants.CMD_JOIN_ROOM, msgRes)
		return
	}

	err = proto.Unmarshal(data, &joinRoomReq)
	if err != nil {
		utils.ResponseWrongStruct(cli.conn, constants.CMD_JOIN_ROOM)
		return
	}

	// get room
	room := cli.server.getRoom(joinRoomReq.Id)
	if room == nil {
		joinRoomRes = tcp_msg_proto.JoinRoomResponse{Status: false, Msg: "Room is not exist"}
		msgRes, _ = proto.Marshal(&joinRoomRes)
		cli.send(constants.CMD_JOIN_ROOM, msgRes)
		return
	}

	go room.joinRoom(cli, joinRoomReq.GetPassword())
}

func handleQuickJoinRoom(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes      []byte
		joinRoomRes tcp_msg_proto.JoinRoomResponse
	)

	if cli.Room != nil {
		joinRoomRes = tcp_msg_proto.JoinRoomResponse{Status: false, Msg: "You are in another room"}
		msgRes, _ = proto.Marshal(&joinRoomRes)
		cli.send(constants.CMD_JOIN_ROOM, msgRes)
		return
	}

	go cli.server.quickJoinRoom(cli)

}

func handleLeftRoom(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes []byte
	)

	if cli.Room == nil {
		msgRes = utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_LEFT_ROOM, msgRes)
		return
	}

	go cli.Room.leftRoom(cli)
}

func handleKickFromRoom(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes []byte
	)
	if cli.Room == nil {
		msgRes = utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_LEFT_ROOM, msgRes)
		return
	}

	go cli.Room.kickOpponent(cli)

}

func handleChatInRoom(cli *Client, msg []byte) {
	defer utils.HandlePanic()

	room := cli.Room

	if room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_LEFT_ROOM, msgRes)
		return
	}

	go room.chat(cli, msg)
}

func handleGetRoomList(cli *Client, data []byte) {
	go cli.server.getRoomListInfo(cli)
}

func handleReadyGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_READY_GAME, msgRes)
		return
	}

	go cli.Room.readyGame(cli)
}

func handleStartGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_START_GAME, msgRes)
		return
	}

	go cli.Room.startGame(cli)

}

func handleMoveGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	if cli.Room.GetRoomStatus() != constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game has not played")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	go cli.Room.moveGame(cli, data)
}

func handleUndoMoveRequest(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	if cli.Room.GetRoomStatus() != constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game has not played")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	go cli.Room.undoMoveRequest(cli, data)
}

func handleUndoMoveResponse(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	if cli.Room.GetRoomStatus() != constants.ROOM_STATUS_PLAYING {
		msgRes := utils.GetActionResponse(false, "Game has not played")
		cli.send(constants.CMD_MOVE_GAME, msgRes)
		return
	}

	go cli.Room.undoMoveResponse(cli, data)
}

func handleSurrenderGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_READY_GAME, msgRes)
		return
	}

	go cli.Room.surrenderGame(cli)
}

func handleWinGame(cli *Client, data []byte) {
	defer utils.HandlePanic()
	if cli.Room == nil {
		msgRes := utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_READY_GAME, msgRes)
		return
	}

	go cli.Room.winGame(cli)
}

func handleChangSideGame(cli *Client, data []byte) {
	defer utils.HandlePanic()

	var (
		msgRes []byte
	)

	if cli.Room == nil {
		msgRes = utils.GetActionResponse(false, "You are not in any room")
		cli.send(constants.CMD_CHANGE_SIDE_GAME, msgRes)
		return
	}

	go cli.Room.changeSide(cli)
}

func handleGetPlayerList(cli *Client, data []byte) {
	// go cli.server.getPlayerListInfo(cli)
}
