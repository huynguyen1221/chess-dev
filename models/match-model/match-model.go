package match_model

import (
	"context"
	"database/sql"
	"errors"
	"mooncity-socket-server/logger"
	mysql_repo "mooncity-socket-server/repositories/mysql"
	"time"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type Match struct {
	Id          int64          `json:"id" db:"id"`
	Player1     sql.NullInt64  `json:"player1" db:"player1"`
	Player2     sql.NullInt64  `json:"player2" db:"player2"`
	ReverseSide bool           `json:"reverseSide" db:"reverseSide"`
	Movement    sql.NullString `json:"movement" db:"movement"`
	Winner      uint8          `json:"winner" db:"winner"`
	Created_at  time.Time      `json:"created_at" db:"created_at"`
	Updated_at  time.Time      `json:"updated_at" db:"updated_at"`
}

var ctx context.Context
var db *sqlx.DB

func init() {
	ctx = context.Background()
	db = mysql_repo.GetInstance()
}

func GetByUserId(userId int64) ([]*Match, error) {
	var matches []*Match
	query := "SELECT * FROM matches WHERE player1 = ? or player2 = ?"
	logger.Info(query)

	err := db.SelectContext(ctx, &matches, query, userId, userId)
	if err != nil {
		logger.Error("", zap.Error(err))
		return nil, err
	}

	return matches, nil
}

func Create(player1 sql.NullInt64, player2 sql.NullInt64, reverseSide bool, movement sql.NullString, winner uint8) (id int64, e error) {
	query := "INSERT INTO matches(player1, player2, reverseSide, movement, winner) VALUES (?,?,?,?,?)"
	logger.Info(query)

	result, e := db.ExecContext(ctx, query, player1, player2, false, movement, winner)
	if e != nil {
		logger.Error("query", zap.Any("create match", e))
		return
	}
	count, _ := result.RowsAffected()
	if count == 0 {
		e = errors.New("Create Match failed")
		return
	}

	id, _ = result.LastInsertId()
	return
}
