package user_model

import (
	"context"
	"errors"
	"fmt"
	"mooncity-socket-server/logger"
	mysql_repo "mooncity-socket-server/repositories/mysql"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type User struct {
	Id         int64     `json:"id" db:"id"`
	Username   string    `json:"username" db:"username"`
	Password   string    `json:"passsword" db:"password"`
	Created_at time.Time `json:"created_at" db:"created_at"`
	Updated_at time.Time `json:"updated_at" db:"updated_at"`
}

type UserSearch struct {
	Id       int64  `json:"id" db:"id"`
	Username string `json:"username" db:"username"`
	Win      int64  `json:"win" db:"win"`
	Lose     int64  `json:"lose" db:"lose"`
}

var ctx context.Context
var db *sqlx.DB

func init() {
	ctx = context.Background()
	db = mysql_repo.GetInstance()
}

func GetById(id int64) (*User, error) {
	var user User
	query := "SELECT * FROM users WHERE id = ?"
	logger.Info(query)

	err := db.GetContext(ctx, &user, query, id)
	if err != nil {
		logger.Error("", zap.Error(err))
		return nil, err
	}

	return &user, nil
}

func SearchById(id int64) (*UserSearch, error) {
	var user UserSearch
	query := fmt.Sprintf(`SELECT id,username, 
	(select count(*) from matches where (player1 = %v and winner = 1) or (player2 = %v and winner = 2)) as win, 
	(select count(*) from matches where (player1 = %v and winner = 2) or (player2 = %v and winner = 1)) as lose 
	FROM users WHERE id = %v`, id, id, id, id, id)
	logger.Info(query)

	err := db.GetContext(ctx, &user, query)
	if err != nil {
		logger.Error("", zap.Error(err))
		return nil, err
	}

	return &user, nil
}

func GetUsersInList(ids []int64) ([]*User, error) {
	var stringIds []string
	for _, id := range ids {
		stringIds = append(stringIds, strconv.Itoa(int(id)))
	}
	var users []*User

	query := "SELECT id, username FROM users WHERE id IN (?)"
	err := db.SelectContext(ctx, &users, query, strings.Join(stringIds, ","))
	if err != nil {
		logger.Error("", zap.Error(err))
		return nil, err
	}

	return users, nil
}

func GetByName(username string) (*User, error) {
	var user User
	query := "SELECT * FROM users WHERE username = ?"
	logger.Info(query)

	err := db.GetContext(ctx, &user, query, username)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

func Create(username string, password string) (id int64, e error) {
	query := "INSERT INTO users(username,password) VALUES (?,?)"
	logger.Info(query)

	result, e := db.ExecContext(ctx, query, username, password)
	if e != nil {
		logger.Error("query", zap.Any("create user", e))
		return
	}
	count, _ := result.RowsAffected()
	if count == 0 {
		e = errors.New("Create user failed")
		return
	}

	id, _ = result.LastInsertId()
	return
}
