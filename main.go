package main

import (
	"fmt"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	"go.uber.org/zap"

	"mooncity-socket-server/config"
	"mooncity-socket-server/logger"

	http_server "mooncity-socket-server/http-server"
	tcp_server "mooncity-socket-server/tcp-server"
)

var cfg *config.Config
var udpConn *net.UDPConn

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())
	// Server for pprof
	go func() {
		fmt.Println(http.ListenAndServe("localhost:6060", nil))
	}()
	cfg = config.GetConfig()
	logger.Info("Start Application: ", zap.Any("cfg", cfg))

	var tcpHandler tcp_server.TcpHandlerClass
	go http_server.Start()
	go tcpHandler.Init()

	time.Sleep(1 * time.Millisecond)
	logger.Info("Handle interrupt working")
	fmt.Println()

	handleInterrupt()
}

func handleInterrupt() {
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	exitChan := make(chan int)
	go func() {
		sig := <-signalChan
		switch sig {
		case syscall.SIGHUP:
			// kill -SIGHUP XXXX
			logger.Info("SIGHUP: Hungup")
			exitChan <- 0
		case syscall.SIGINT:
			// kill -SIGINT XXXX or Ctrl+c
			logger.Info("SIGINT: Interrupt")
			exitChan <- 0
		case syscall.SIGTERM:
			// kill -SIGTERM XXXX
			logger.Info("SIGTERM: Force stop")
			exitChan <- 0
		case syscall.SIGQUIT:
			// kill -SIGQUIT XXXX
			logger.Info("SIGQUIT: Stop and core dump")
			exitChan <- 0
		default:
			logger.Info("Unknown signal.")
			exitChan <- 1
		}
	}()

	code := <-exitChan
	logger.Info("Graceful Exit!")
	os.Exit(code)
}
