package http_server

import (
	"encoding/json"
	"fmt"
	"io"
	"mooncity-socket-server/constants"
	user_controller "mooncity-socket-server/controllers/user-controller"
	"mooncity-socket-server/dto"
	"mooncity-socket-server/logger"
	match_model "mooncity-socket-server/models/match-model"
	user_model "mooncity-socket-server/models/user-model"
	"mooncity-socket-server/utils"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/gorilla/mux"
)

var httpMutex sync.RWMutex
var routes = mux.NewRouter()

type ResponseObject struct {
	Data any
}

func initRoutes() {
	routes.HandleFunc("/", homePage).Methods("GET")
	routes.HandleFunc("/login", login).Methods("POST")
	routes.HandleFunc("/signup", signup).Methods("POST")
	routes.HandleFunc("/users/{id:[0-9]+}", getUserInfo).Methods("GET")
	routes.HandleFunc("/users/{id:[0-9]+}/matches", getUserMatches).Methods("GET")
	routes.HandleFunc("/check", check).Methods("POST")
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Home Page")
}
func login(w http.ResponseWriter, r *http.Request) {
	var body dto.UserRequest

	data, _ := io.ReadAll(r.Body)
	json.Unmarshal(data, &body)
	fmt.Println(body)

	user, e := user_controller.Login(body)
	if e != nil {
		w.WriteHeader(400)
		return
	}

	// generate jwt token
	token, _ := utils.GetnerateJWTKey(user.Id)

	resData := dto.LoginResponse{User: dto.UserInfo{Id: user.Id, Username: user.Username}, Token: token}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resData)
}

func signup(w http.ResponseWriter, r *http.Request) {
	data, _ := io.ReadAll(r.Body)

	var body dto.UserRequest
	json.Unmarshal(data, &body)

	body.Username = strings.Trim(body.Username, " ")
	body.Password = strings.Trim(body.Password, " ")

	if body.Username == "" || body.Password == "" {
		w.WriteHeader(400)
		return
	}

	userId, e := user_controller.Signup(body)
	if e != nil {
		logger.Error(e.Error())
		w.WriteHeader(400)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ResponseObject{Data: userId})
}

func getUserInfo(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		w.WriteHeader(400)
		return
	}
	user, err := user_model.SearchById(int64(id))
	if err != nil {
		w.WriteHeader(404)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(user)
}

func getUserMatches(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		w.WriteHeader(400)
		return
	}
	_, err = user_model.GetById(int64(id))
	if err != nil {
		w.WriteHeader(404)
		return
	}

	matches, err := match_model.GetByUserId(int64(id))
	if err != nil {
		json.NewEncoder(w).Encode([]int{})
		return
	}

	matchesRes := []dto.Matches{}
	opponentIds := []int64{}
	for _, m := range matches {
		match := dto.Matches{Id: m.Id, Opponent: dto.Opponent{}, White: true, IsWin: true, Time: m.Created_at}

		if (m.Player1.Valid && m.Player1.Int64 == int64(id) && m.ReverseSide) ||
			(m.Player2.Valid && m.Player2.Int64 == int64(id) && !m.ReverseSide) {
			match.White = false
		}

		if (m.Winner == constants.PLAYER_1 && m.Player2.Valid && m.Player2.Int64 == int64(id)) ||
			(m.Winner == constants.PLAYER_2 && m.Player1.Valid && m.Player1.Int64 == int64(id)) {
			match.IsWin = false
		}

		if !m.Player1.Valid || !m.Player2.Valid {
			match.Opponent.Username = "guest"
		} else {
			match.Opponent.IsAuth = true
		}

		if match.Opponent.IsAuth {
			if m.Player1.Int64 == int64(id) {
				match.Opponent.Id = m.Player2.Int64
				opponentIds = append(opponentIds, m.Player2.Int64)
			} else {
				match.Opponent.Id = m.Player1.Int64
				opponentIds = append(opponentIds, m.Player1.Int64)
			}
		}

		matchesRes = append(matchesRes, match)
	}

	// opponentIds
	opponents, _ := user_model.GetUsersInList(opponentIds)

	for i := 0; i < len(matchesRes); i++ {
		match := &matchesRes[i]

		if match.Opponent.IsAuth {
			for _, opponent := range opponents {
				if match.Opponent.Id == opponent.Id {
					fmt.Println("opponenet: ", opponent.Username)
					(&match.Opponent).Username = opponent.Username
					fmt.Println("opponent: ", match)
					match.Opponent.Id = 11

					break
				}
			}
		}
	}

	// matchesRes[0].Id = 20

	fmt.Println(matchesRes)

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(matchesRes)
}

func check(w http.ResponseWriter, r *http.Request) {
	var token *string

	data, _ := io.ReadAll(r.Body)
	json.Unmarshal(data, &token)
	fmt.Println(*token)

	claims, _ := utils.ParseJWT(*token)
	fmt.Println((*claims).UserID)
}
