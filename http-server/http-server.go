package http_server

import (
	"log"
	"mooncity-socket-server/config"
	"mooncity-socket-server/logger"
	"net/http"
)

var (
	cfg *config.Config
)

func init() {
	cfg = config.GetConfig()
}

func Start() {
	logger.Info("Listening HTTP on " + "localhost:" + cfg.HttpServerPort)

	initRoutes()
	log.Fatal(http.ListenAndServe(cfg.ServerHost+":"+cfg.HttpServerPort, routes))
}
