package user_controller

import (
	"errors"
	"mooncity-socket-server/dto"
	"mooncity-socket-server/logger"
	user_model "mooncity-socket-server/models/user-model"

	"golang.org/x/crypto/bcrypt"
)

func Login(userRequest dto.UserRequest) (user *user_model.User, e error) {
	logger.Object(userRequest)
	user, e = user_model.GetByName(userRequest.Username)
	if e != nil {
		logger.Error("User not found")
		return
	}

	e = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(userRequest.Password))
	if e != nil {
		logger.Error("Wrong password")
		return
	}

	return
}

func Signup(userRequest dto.UserRequest) (userId int64, e error) {
	logger.Object(userRequest)

	_, e = user_model.GetByName(userRequest.Username)

	if e == nil {
		e = errors.New("username is already existed")
		return
	}

	hashedPass, e := bcrypt.GenerateFromPassword([]byte(userRequest.Password), 10)
	if e != nil {
		return
	}

	userRequest.Password = string(hashedPass)
	userId, e = user_model.Create(userRequest.Username, userRequest.Password)

	return
}
